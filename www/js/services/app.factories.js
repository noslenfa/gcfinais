angular.module('gcFinaisApp.factories', [])

  .factory('MultiGeocaches', function ($http, apiUrlMulti) {
    var MultiGeocaches = {};

    MultiGeocaches.getAllMulti = function (callback) {
      return $http.get(apiUrlMulti).then(callback);
    };

    MultiGeocaches.getMultiByParams = function (params, callback) {
      return $http.get(apiUrlMulti + params).then(callback);
    };

    return MultiGeocaches;

  })

  .factory('MysteryGeocaches', function ($http, apiUrlMystery) {
    var MysteryGeocaches = {};

    MysteryGeocaches.getAllMystery = function (callback) {
      return $http.get(apiUrlMystery).then(callback);
    };

    MysteryGeocaches.getMysteryByParams = function (params, callback) {
      return $http.get(apiUrlMystery + params).then(callback);
    };

    return MysteryGeocaches;

  })
