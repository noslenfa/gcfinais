angular.module('gcFinaisApp', ['ionic',

    'gcFinaisApp.controllers',
    'gcFinaisApp.factories',
    'gcFinaisApp.configs',

    'ngCordova',

    'LocalStorageModule'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {

        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

    });
  })

  .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
    // main app state
      .state('app', {
        url: '/app',
        cache: false,
        abstract: true,
        templateUrl: 'templates/main.html',
        controller: 'AppMainCtrl'
      })

      .state('app.init', {
        url: '/init',
        cache: false,
        views: {
          'mainContent': {
            templateUrl: 'templates/init.html',
            controller: 'AppMainCtrl'
          }
        }
      })

      .state('app.multi', {
        url: '/multi',
        cache: false,
        views: {
          'mainContent': {
            templateUrl: 'templates/multi.html',
            controller: 'AppMainCtrl'
          }
        }
      })

      .state('app.mystery', {
        url: '/mystery',
        cache: false,
        views: {
          'mainContent': {
            templateUrl: 'templates/mystery.html',
            controller: 'AppMainCtrl'
          }
        }
      })

      .state('app.multidescription', {
        url: '/multidescription/:gcID',
        cache: false,
        views: {
          'mainContent': {
            templateUrl: 'templates/multidescription.html',
            controller: 'MultiDescriptionCtrl'
          }
        }
      })

      .state('app.mysterydescription', {
        url: '/mysterydescription/:gcID',
        cache: false,
        views: {
          'mainContent': {
            templateUrl: 'templates/mysterydescription.html',
            controller: 'MysteryDescriptionCtrl'
          }
        }
      });

    $urlRouterProvider.otherwise('/app/init');
  })
