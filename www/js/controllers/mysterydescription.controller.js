'use strict';

/**
 * @ngdoc function
 * @name mysteryfinais.controller
 * @description
 * # MysteryDescriptionCtrl
 * Controller of the GCFinais
 */

angular.module('gcFinaisApp.controllers')

  .controller('MysteryDescriptionCtrl', function ($scope, $rootScope, $stateParams) {

    $scope.mystery_index = $stateParams.gcID;

    var coordenadas = $scope.all_mystery[$scope.mystery_index].coordenadas;

    var url_gc = 'https://www.geocaching.com/geocache/'+$scope.all_mystery[$scope.mystery_index].gc;

    var name = $scope.all_mystery[$scope.mystery_index].inseriu;

    $scope.notes = $scope.all_mystery[$scope.mystery_index].notas;

    switch(name) {
      case "NELSON":
        $scope.img_name = "noslenfa.png";
        $scope.telf = "00351934521982";
        break;
      case "PAULO":
        $scope.img_name = "pmendes.png";
        $scope.telf = "00351924412680";
        break;
      case "EDUARDO":
        $scope.img_name = "eduest.png";
        $scope.telf = "0035196978190";
        break;
      case "RUI":
        $scope.img_name = "rf32.png";
        $scope.telf = "00351967584680";
        break;
      case "BIRI@TUS":
        $scope.img_name = "biriatus.png";
        $scope.telf = "nao";
        break;
      default:
        $scope.img_name = "dunno.png";
        $scope.telf = "nao";
        break;
    }

    function ConvertDMSToDD(degrees, minutes, direction) {
      var dd = parseFloat(degrees) + parseFloat(minutes) / 60;

      if (direction == "S" || direction == "W") {
        dd = dd * -1;
      } // Don't do anything for N or E
      return dd;
    }

    var parts = coordenadas.split(/[^\d\w]+/);

    var lat_submin = '0.' + parts[3];
    var lat_min = +parts[2] + +lat_submin;
    var lng_submin = '0.' + parts[7];
    var lng_min = +parts[6] + +lng_submin;

    var lat = ConvertDMSToDD(parts[1], lat_min, parts[0]);
    var lng = ConvertDMSToDD(parts[5], lng_min, parts[4]);

    $scope.newLat = lat;
    $scope.newLng = lng;
    $scope.position = lat + ',' + lng;

    var myLatLng = new google.maps.LatLng(41.3959974, -8.5350965);

    var mapOptions = {
      center: myLatLng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("the-map"), mapOptions);

    map.setCenter({
      lat: $scope.newLat,
      lng: $scope.newLng
    });

    var myLatLngF = new google.maps.LatLng($scope.newLat, $scope.newLng);

    var marker = new google.maps.Marker({
      position: myLatLngF,
      icon: 'img/mystery_icon.svg',
      map: map
    });

    $scope.openExternal = function() {
      window.open(url_gc, '_system', 'location=yes');
      return false; // Prevent execution of the default onClick handler
    }

  })
