'use strict';

/**
 * @ngdoc function
 * @name gcfinais.controller
 * @description
 * # AppMainCtrl
 * Controller of the GCFinais
 */
angular.module('gcFinaisApp.controllers', [])

  .controller('AppMainCtrl', function ($scope, $state, $rootScope, $ionicPlatform, $cordovaNetwork, localStorageService, MultiGeocaches, MysteryGeocaches) {

    $scope.is_back=true;

    var element;
    $rootScope.$on('$locationChangeSuccess', function (event, newUrl) {
      var new_url = newUrl;
      element = angular.element(document.querySelectorAll('ion-header-bar'));

      if (new_url.indexOf('mystery') > 0) {
        $scope.is_back_mystery=false;
        $scope.is_back_multi=false;
        if (new_url.indexOf('mysterydescription') > 0) {
          $scope.is_back_mystery=true;
        } else {
          $scope.is_back_mystery=false;
          $scope.is_mystery = false;
          $scope.is_multi = true;
          $scope.not_home = true;
          element.removeClass('bar-main');
          element.removeClass('bar-multi');
          element.addClass('bar-mystery');
          $scope.topTitle = 'Finais das Mistério';
        }
      } else if (new_url.indexOf('multi') > 0) {
        $scope.is_back_multi=false;
        $scope.is_back_mystery=false;
        if (new_url.indexOf('multidescription') > 0) {
          $scope.is_back_multi=true;
        } else {
          $scope.is_back_multi=false;
          $scope.is_mystery = true;
          $scope.is_multi = false;
          $scope.not_home = true;
          element.removeClass('bar-main');
          element.removeClass('bar-mystery');
          element.addClass('bar-multi');
          $scope.topTitle = 'Finais das Multi';
        }
      } else {
        $scope.is_back=true;
        $scope.is_mystery = false;
        $scope.is_multi = false;
        $scope.not_home = false;
        element.removeClass('bar-mystery');
        element.removeClass('bar-multi');
        element.addClass('bar-main');
        $scope.topTitle = 'GC Finais';
      }

    });

    $scope.$on('$ionicView.beforeEnter', function() {
      $rootScope.viewColor = 'darkgreen';
      $rootScope.topTitle = 'GC Finais';
    });

    $scope.hasInternet = true;

    if (window.cordova) {
      $ionicPlatform.ready(function () {
        $scope.hasInternet = $cordovaNetwork.isOnline();
        init();
      });

      $rootScope.$on('$cordovaNetwork:online', function () {
        $scope.hasInternet = true;
      });

      $rootScope.$on('$cordovaNetwork:offline', function () {
        $scope.hasInternet = false;
      });
    } else {
      init();
    }

    function init() {
      if ($scope.hasInternet) {
        MultiGeocaches.getAllMulti(function (data) {
          $scope.all_multi = data.data.result;
          localStorageService.set('all_multi', $scope.all_multi);
        });
        MysteryGeocaches.getAllMystery(function (data) {
          $scope.all_mystery = data.data.result;
          localStorageService.set('all_mystery', $scope.all_mystery);
        });
      } else {
        $scope.all_multi = localStorageService.get('all_multi');
        $scope.all_mystery = localStorageService.get('all_mystery');
      }
    };

    $scope.gcGo = function (index, type) {
      if (type === 'multi') {
        $state.go('app.multidescription', {
          'gcID': index
        })
      } else if (type === 'mystery') {
        $state.go('app.mysterydescription', {
          'gcID': index
        })
      }

    };

  })
